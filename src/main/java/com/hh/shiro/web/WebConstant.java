package com.hh.shiro.web;

public class WebConstant {
	
	public static final String SESSOIN_AUTH_ERROR_MSG = "SESSOIN_AUTH_ERROR_MSG";
	
	public static final String SESSOIN_SMS_VALIDATE_PASS = "SESSOIN_SMS_VALIDATE_PASS";
	
	public static final String SESSOIN_SMS_ERROR_MSG = "SESSOIN_SMS_ERROR_MSG";
	
	public static final String SESSION_UC_MOBILEPHONE = "SESSION_UC_MOBILEPHONE";

}
